from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'nickname', 'email')


class LoginUserSerializer(serializers.Serializer):
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Unable to log in with provided credentials.")


class RegisterUserSerializer(serializers.Serializer):
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    email = serializers.CharField()
    password = serializers.CharField()
    nickname = serializers.CharField()

    def validate(self, data):
        User = get_user_model()
        if User.objects.filter(email=data['email']).exists():
            raise serializers.ValidationError('이미 가입된 이메일입니다.')
        elif User.objects.filter(nickname=data['nickname']).exists():
            raise serializers.ValidationError('이미 존재하는 닉네임입니다.')
        user = User.objects.create(
            email=data['email'],
            username=data['email'],
            nickname=data['nickname']
        )
        user.set_password(data['password'])
        user.save()
        return user
