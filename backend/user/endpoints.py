from django.conf.urls import include
from django.urls import path
from rest_framework import routers

from user.views import LoginAPI, UserAPI, RegistrationAPI

router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    path('auth/login/', LoginAPI.as_view()),
    path('auth/user/', UserAPI.as_view()),
    path('auth/register/', RegistrationAPI.as_view()),
]
