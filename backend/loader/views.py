import logging

from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from meta.views import Meta


class LoaderView(View):
    """
    Serves the compiled frontend entry point (only works if you have run `yarn
    run build`).
    """

    def get(self, request):
        try:
            return render(request, 'index.html', {'meta': self.get_meta(request)})
        except FileNotFoundError:
            logging.exception('Production build of app not found')
            return HttpResponse(
                """
                This URL is only used when you have built the production
                version of the app. Visit http://localhost:3000/ instead, or
                run `yarn run build` to test the production version.
                """,
                status=501,
            )

    def get_meta(self, request):
        return Meta(
            title=self.get_title_from_urlpath(request),
        )

    @staticmethod
    def get_title_from_urlpath(request):
        if request.path_info.find('/login') == 0:
            title = '로그인 | Project'
        else:
            title = 'Project'
        return title
