export const register = (email, nickname, password) => {
  return (dispatch, getState) => {
    let headers = {"Content-Type": "application/json"};
    let body = JSON.stringify({email, nickname, password});

    return fetch("/auth/register/", {headers, body, method: "POST"})
      .then(res => {
        if (res.status === 200) {
          res.json().then(data => {
            dispatch({type: 'REGISTRATION_SUCCESSFUL', data});
            return data;
          });
        } else if (res.status === 403 || res.status === 401) {
          dispatch({type: "REGISTRATION_ERROR", data: res.data});
          throw res.data;
        } else {
          dispatch({type: "REGISTRATION_FAILED", data: res.data});
          throw res.data;
        }
      });
  }
};
