import * as auth from "./auth";
import * as registration from "./registration";

export {auth, registration}
