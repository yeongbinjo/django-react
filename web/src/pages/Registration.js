import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import RegistrationForm from "../components/RegistrationForm";
import { connect } from "react-redux";
import Link from "@material-ui/core/Link";

const styles = theme => ({
  root: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

class Registration extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <div className={classes.root}>
          {
            this.props.isRegistered ? (
              <div>
                가입을 환영합니다.
                <Link to='/login'>로그인하기</Link>
              </div>
            ) : (
              <RegistrationForm />
            )
          }
        </div>
      </React.Fragment>
    );
  }
}

Registration.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  let errors = [];
  if (state.auth.errors) {
    errors = Object.keys(state.auth.errors).map(field => {
      return {field, message: state.auth.errors[field]};
    });
  }
  return {
    errors,
    isRegistered: state.register.isRegistered,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Registration));
