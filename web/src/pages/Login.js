import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import LoginForm from "../components/LoginForm";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";

const styles = theme => ({
  root: {
    height: '100vh',
    [theme.breakpoints.down('xs')]: {
      height: 'calc(100vh - 56px)',
    },

    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: theme.spacing.unit * 2,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  logo: {
    width: 300,
    height: 250,
    '@media (max-height:500px)': {
      display: 'none'
    },
  },
  fab: {
    position: 'absolute',
    left: theme.spacing.unit,
    top: theme.spacing.unit,
    opacity: 0.5,
  }
});

class LoginPage extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <div className={classes.root}>
          <Paper className={classes.box} elevation={16}>
            <LoginForm />
          </Paper>
        </div>
      </React.Fragment>
    );
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(LoginPage));
