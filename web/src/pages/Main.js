import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Header from "../components/Header";

const styles = (theme) => ({
  content: {
    width: 'auto',
    [theme.breakpoints.up('lg')]: {
      width: 1280,
    },
    padding: theme.spacing.unit * 2,
    margin: 'auto',
  },
  rhombuses: {
    paddingTop: 100,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  row: {
    display: 'flex',
    justifyContent: 'center',
    margin: -70
  },
  rhombus: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    display: 'inline-flex',
    width: 200,
    height: 200,
    margin: 42,
    transform: 'rotate(45deg)',
  }
});

class Main extends React.Component {
  generate = (element) => {
    return [0, 1, 2].map(value =>
      React.cloneElement(element, {
        key: value,
      }),
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Header />
        <div className={classes.content}>
          <div className={classes.rhombuses}>
            <div className={classes.row}>
              <div className={classes.rhombus}></div>
              <div className={classes.rhombus}></div>
              <div className={classes.rhombus}></div>
            </div>
            <div className={classes.row}>
              <div className={classes.rhombus}></div>
              <div className={classes.rhombus}></div>
            </div>
            <div className={classes.row}>
              <div className={classes.rhombus}></div>
              <div className={classes.rhombus}></div>
              <div className={classes.rhombus}></div>
            </div>
            <div className={classes.row}>
              <div className={classes.rhombus}></div>
              <div className={classes.rhombus}></div>
            </div>
            <div className={classes.row}>
              <div className={classes.rhombus}></div>
              <div className={classes.rhombus}></div>
              <div className={classes.rhombus}></div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Main);