import { combineReducers } from 'redux';
import auth from "./auth";
import registration from "./registration";


const reducer = combineReducers({
  auth,
  registration
});

export default reducer;
