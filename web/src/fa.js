import { library } from '@fortawesome/fontawesome-svg-core'
import { faIgloo } from '@fortawesome/free-solid-svg-icons'

const loadFontAwesome = () => {
  library.add(faIgloo);
};

export default loadFontAwesome;
