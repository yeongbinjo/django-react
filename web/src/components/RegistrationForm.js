import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { registration } from "../actions";
import ReactDOM from 'react-dom';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { Redirect } from "react-router-dom";
import green from "@material-ui/core/colors/green";
import grey from "@material-ui/core/colors/grey";
import Button from "@material-ui/core/Button";

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  formControl: {
    margin: theme.spacing.unit,
  },
  cssLabel: {
    '&$cssFocused': {
      color: green[500],
    },
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: green[500],
    },
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: green[500],
    },
    '&$cssFocused': {
      color: green[500]
    }
  },
  cssOutlinedPasswordInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: green[500],
    },
    '&$cssFocused': {
      fontFamily: 'Roboto',
      color: green[500]
    },
    fontFamily: 'Roboto',
    color: grey[500]
  },
  notchedOutline: {},
  margin: {
    margin: theme.spacing.unit
  }
});

class InvitationForm extends React.Component {
  state = {
    email: "",
    nickname: "",
    password: "",
    passwordConfirm: "",
  };

  onSubmit = (e) => {
    e.preventDefault();
    if (this.state.password === this.state.passwordConfirm) {
      this.props.register(this.state.email, this.state.nickname, this.state.password);
    }
  };

  catchReturn = (e) => {
    if (e.key === 'Enter') {
      this.onSubmit(e);
    }
  };

  onFailure = (response) => {
    console.log(response);
  };

  render() {
    const { classes } = this.props;
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }
    return (
      <div className={classes.root}>
        <form
          className={classes.container}
          noValidate
          autoComplete="off"
          onSubmit={this.onSubmit}
        >
          <FormControl className={classes.formControl} variant="outlined">
            <InputLabel
              ref={ref => {
                this.labelRef = ReactDOM.findDOMNode(ref);
              }}
              classes={{
                root: classes.cssLabel,
              }}
              htmlFor="email-outlined"
            >
              계정
            </InputLabel>
            <OutlinedInput
              id="email-outlined"
              onChange={e => this.setState({email: e.target.value})}
              labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
              onKeyPress={this.catchReturn}
              classes={{
                root: classes.cssOutlinedInput,
                focused: classes.cssFocused,
                notchedOutline: classes.notchedOutline,
              }}
              autoComplete="email"
            />
          </FormControl>
          <FormControl className={classes.formControl} variant="outlined">
            <InputLabel
              ref={ref => {
                this.labelRef = ReactDOM.findDOMNode(ref);
              }}
              classes={{
                root: classes.cssLabel,
              }}
              htmlFor="nickname-outlined"
            >
              닉네임
            </InputLabel>
            <OutlinedInput
              id="nickname-outlined"
              onChange={e => this.setState({nickname: e.target.value})}
              labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
              onKeyPress={this.catchReturn}
              classes={{
                root: classes.cssOutlinedInput,
                focused: classes.cssFocused,
                notchedOutline: classes.notchedOutline,
              }}
              autoComplete="nickname"
            />
          </FormControl>
          <FormControl className={classes.formControl} variant="outlined">
            <InputLabel
              ref={ref => {
                this.labelRef = ReactDOM.findDOMNode(ref);
              }}
              classes={{
                root: classes.cssLabel,
              }}
              htmlFor="password-outlined"
            >
              비밀번호
            </InputLabel>
            <OutlinedInput
              id="password-outlined"
              type="password"
              onChange={e => this.setState({password: e.target.value})}
              labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
              onKeyPress={this.catchReturn}
              classes={{
                root: classes.cssOutlinedPasswordInput,
                focused: classes.cssFocused,
                notchedOutline: classes.notchedOutline,
              }}
              autoComplete="new-password"
            />
          </FormControl>
          <FormControl className={classes.formControl} variant="outlined">
            <InputLabel
              ref={ref => {
                this.labelRef = ReactDOM.findDOMNode(ref);
              }}
              classes={{
                root: classes.cssLabel,
              }}
              htmlFor="password-confirm-outlined"
            >
              비밀번호 확인
            </InputLabel>
            <OutlinedInput
              id="password-confirm-outlined"
              type="password"
              onChange={e => this.setState({passwordConfirm: e.target.value})}
              labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
              onKeyPress={this.catchReturn}
              classes={{
                root: classes.cssOutlinedPasswordInput,
                focused: classes.cssFocused,
                notchedOutline: classes.notchedOutline,
              }}
              autoComplete="new-password"
            />
          </FormControl>
          <Button variant="outlined" size="large" color="primary" className={classes.margin}
            onClick={this.onSubmit}>
            가입하기
          </Button>
        </form>
      </div>
    );
  }
}

InvitationForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  let errors = [];
  if (state.auth.errors) {
    errors = Object.keys(state.auth.errors).map(field => {
      return {field, message: state.auth.errors[field]};
    });
  }
  return {
    errors,
    isAuthenticated: state.auth.isAuthenticated
  };
};

const mapDispatchToProps = dispatch => {
  return {
    register: (email, nickname, password) => {
      return dispatch(registration.register(email, nickname, password));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(InvitationForm));
